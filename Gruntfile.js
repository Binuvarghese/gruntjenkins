module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    folders: {
      webapp: {
        root: '/Program Files (x86)/Jenkins/jobs/bit/workspace',
        build: '/Devops/grunt/dest'
      }
    },

   copy: {
      javafiles: {
        expand: true,
        cwd: '<%= folders.webapp.root %>/src/java/',
        src: ['**/*'],
        dest: '<%= folders.webapp.build %>/java/'
      },
      pythonfiles: {
        expand: true,
        cwd: '<%= folders.webapp.root %>/src/python/',
        src: ['**/*'],
        dest: '<%= folders.webapp.build %>/python/'
      },
      csharpfiles: {
        expand: true,
        cwd: '<%= folders.webapp.root %>/src/csharp/',
        src: ['**/*'],
        dest: '<%= folders.webapp.build %>/csharp/'
      }      
    },
   
  run_java: {
    options: { //Default is true 
      stdout: true,
      stderr: true,
      stdin: true,
      failOnError: false
    },
    javac_task: {
      execOptions:{
        cwd: '<%= folders.webapp.build %>/java/'
      }, 
      command: "javac",     
      sourceFiles: ["/Devops/grunt/dest/java/*.java"]
    },
	jar_task: {
      execOptions:{
        cwd: '<%= folders.webapp.build %>/java/'
      },
      command: "jar",
      jarName: "hello.jar",
      jarOptions : "cvf", 
      manifestName: "MANIFEST",
      dir: "/Devops/grunt/dest/java/",
      files: "*.class"
    }
  }   

  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-run-java');

  grunt.registerTask('default', [ 'copy','run_java']);

};